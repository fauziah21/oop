<?php

  require_once 'animal.php';
  require_once 'frog.php';
  require_once 'ape.php';

  $sheep = new animal('shaun');

  echo "Namanya adalah " . $sheep->name . "<br>";
  echo "Jumlah Kakinya ". $sheep->legs . "<br>";
  echo "Apakah berdarah dingin? ".$sheep->cold_blooded. "<br><br>";

  $sungokong = new ape("kera sakti");
  $sungokong->yell(); // "Auooo"
  echo "Namanya adalah " . $sungokong->name . "<br>";
  echo "Jumlah Kakinya ". $sungokong->legs . "<br>";
  echo "Apakah berdarah dingin? ".$sungokong->cold_blooded. "<br><br>";


  $kodok = new frog("buduk");
  $kodok->jump() ; // "hop hop"
  echo "Namanya adalah " . $kodok->name . "<br>";
  echo "Jumlah Kakinya ". $kodok->legs . "<br>";
  echo "Apakah berdarah dingin? ".$kodok->cold_blooded. "<br>";

 ?>
